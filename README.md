# Workshops

This repository contains workshops in R. Each workshop contains pedagogical scripts with comments for step-by-step understanding of the applied techniques.

## Current workshops

 - **Brussels 20km** Scraping results from the Brussels running event to produce descriptive statistics

 - **Dreidl:** Simulating Draidle games in R to determine some underlying statistics of this classical Hanukkah game. 

 - **Eulerisation:** Step by step function to modify graph towards graph with eulerian cycle.

 - **Eurovision Cluster:** Mine Eurovision score boards and make network cluster analysis of voting patterns.

 - **Knight's tour:** Applying itterative rules to find valid solutions to this [classical chess-problem](https://en.wikipedia.org/wiki/Knight%27s_tour)

 - **Montecarlo:** Various explorations of montecarlo concepts in R.

 - **Simple web scraping:** Use R and or Python to scrape data from the web.

 - **Swedish Elections 2022:** Descriptives and basic geographical models for analysing the 2022 election results.

 - **Teaching Python:** A loose collection of sample code to explain concepts in Python to beginners and post-beginners. 

   

   