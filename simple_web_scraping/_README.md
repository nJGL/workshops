### Web scraping example files for SamHack 25/4 2018
These scripts were originally posted as code-examples for a workshop at Umeå University's SamHack group of code-oriented PhD-students in social sciences. These scripts demonstrate elementary methods of scraping web-pages for data.

#### Same script in Python and R
The code performs more or less the same actions in Python and R. Most code-lines are steps named 1:1, 1:2 etc. The python-script has a corresponding step 1:1 in R and vice versa. The reason for this dual coding was that different workshop members came from different coding backgrounds and would be able to look at the new topic of scraping in their native tongue.

Hopefully, the scripts could also be re-read while learning Python from R or the other way around.

#### File structure
Short description of the following files:

  - **_info.md** - This file.
  - **scrape_school.R** - Basic scraping in R with pedagogical comments.
  - **scrape_schoo.py** - Basic scraping in Python with pedagogical comments.
  - **pattern_umu_roll.xml** - The repeated pattern for data presentation of umu employees.
  - **pattern_umu_roll_clean.xml** - Te same pattern in cleanly indented xml.
