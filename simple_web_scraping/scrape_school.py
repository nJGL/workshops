""" Web scraping with SamHack 25/4 2018. """
""" nJGL 2018 - This code is free for all to use and modyfy for any purpose. """

""" STEP 1 """
# We will scrape the deprtment catalogue of employees.
# This data is here:
# 1:1
victim_url = 'http://www.geoekhist.umu.se/om-institutionen/personal'


""" STEP 2 """
# Scraping uses these libraries in python
from lxml import html
import requests


""" STEP 3 """
# 3:1
# Use requests and lxml to grab a DOM-tree.
tree = html.fromstring(requests.get(victim_url).content)

# 3:2
# Look at what kind of data we got
type(tree)

# 3:3
# Send a http-request to get the data from the web-server
recieved_data = requests.get(victim_url)
# get the html as a long text
recieved_html = recieved_data.content
# 3:4
# All the html of the victim page is here
recieved_html
# 3:5
# make a DOM-tree from the text. These tree-objects can later be used to
# fetch data using xpath.
tree = html.fromstring(recieved_html)


""" STEP 4 """
# Look at the victim url.
# (html from one entry is in 'oattern_umu_roll_clean.xml')
#
# Does it seem to be a web page that loads all the data from the initial
# http-request, or is there any ajax involved. If you can see all the data
# you would want in the source-code, this is generally a good sign.
#
# Identify the xml-structure which would lead you to the data-points that you
# are interested in. This example contains no nested loops. For each scrapable
# unit, there are only single data-points like name, email, picture and so on.

# The xpath we're using have this following syntax:
# // (start at the begining of the tree)
# span (match a span-tag)
# [@class] (match span tags with the attribute class)
# ="personName" (match span-tags with class set to "personName")

# 1) We want the name of the employee.
# The name is stored in a <span>-tag with the attribute class set to
# "personName" inside a <div>-tag with class set to "personInfo".
# 4:1
names = tree.xpath('//span[@class="personName"]')

# 4:2
# xpath() returns a list of tag-objects, one for each matching tag.
names

# 4:3
# Access the text between the tag's start- and end-tag using .text
# look at a name
a_name = names[0]
a_name.text

# 4:4
# Look att all the names:
[name.text for name in names]


# 2) We want the email address
# The raw email adress is inside a span-tag without any attributes. It is,
# however, nested inside an <a>-tag with the class set to both "mail" and
# "linkableItem"
# 4:6
emails = tree.xpath('//a[@class="mail linkableItem"]/span')
# This match is identical:
emails = tree.xpath('//a[starts-with(@class,"mail")]/span')

# 4:7
# Look at all the emails
[email.text for email in emails]

# 4:8
# But at this point, there is an error in our scraping!
len(emails) == len(names)

# Some people have no email adresses. The number of finds are uneven.
# It is essential to treat missing data as a part of the scraping process.
# We will solve this by collecting each person on a per-person basis in a loop
# to make sure to get missing data too.

""" STEP 5 """
# Prepare to store the data that we want to fetch. In Python we use pandas.
# 5:1
import pandas as pd

# 5:2
# The final output will be stored in
df = pd.DataFrame()


""" STEP 6 """
# To collect data on a per-individual basis. We'll find structures for
# each individual, loop through them, and collect available info on a per-
# individual basis.

# 6:1
# How many employees are there?
employee_ids = tree.xpath('//div[@class="personContainer"]/@id')
len(employee_ids)

# NOTA BENE
# The xpath-request was for the id-attribute of each personContainer-tag.
# Since the id-attribute seems to contain the CAS-login-name, we can hope
# that it should uniquely identify the people.

# 6:2
# The same result could have been gotten this way
employees = tree.xpath('//div[@class="personContainer"]')
employee_ids = [employee.attrib['id'] for employee in employees]
employee_ids
# Quizz yourself: Why do you think the first version is nicer than the second?


# 6:5
# The important thing is that we now have a list of employees by id:s which
# we can loop to find all the info we need on a per-person basis:
employee_ids


""" STEP 7 """
# 7:1
# Setup to try one individual in the loop
employee_id = employee_ids[0]

# 7:2
for employee_id in employee_ids:
    # We can continue to query employess with further xpath questions

    # The xpaths will start to be a bit complicated. Everytime we look for
    # something, we'll want tags that are INSIDE a <div class="personContainer">
    # with the id set to employee_id.

    # 7:3
    # All xpaths should start with
    base_xpath = '//div[@class="personContainer" and @id="' + \
        employee_id + '"]'

    # 7:4
    # These are the xpaths of the data we're interested in
    patterns = {'name': base_xpath + '//span[@class="personName"]',
                'email': base_xpath + '//a[@class="mail linkableItem"]/span',
                'title': base_xpath + '//span[@class="personTitle"]',
                'phone': base_xpath + '//a[@class="phone listableItem"]/span'}

    # 7:6
    # Look at the xpaths of the data we'll scrape
    patterns

    # 7:7
    # One employee's data can be accessed like this
    name = tree.xpath(patterns['name'])
    name[0].text

    # 7:8
    # Iterate to get all the data at once.
    data = [tree.xpath(patterns[variable]) for variable in patterns]
    # Quiz yourself: data contains a list of what?,
    # Quiz yourself: What happens if the xpath doesn't match anything

    # 7:9
    # Empty matches appear in the list as empty lists: []
    # In order to keep the missing data structure, we save nonmissing data and
    # use an empty string ('') in the place wher missing data would have been.
    nonmissing_data = [match[0].text if match != [] else '' for match in data]

    # 7:10
    # Append this observation to the final output
    new_row = pd.DataFrame([[employee_id, *nonmissing_data]])
    df = df.append(new_row)

# 7:11
# Adjust dataframe column-names
df.columns = ['CAS'] + list(patterns.keys())

# 7:12
# Boom
df
