# nJGL 2020-03-22 (in memoriam My)

def is_prime(x):
	""" Is [x] a prime? return false or int."""
	for i in range(2, int(x ** 0.5) + 1):
		if x % i == 0:
			return False
	return x

def list_primes(lim=10):
	""" Yield prime numbers below [limit]. """
	for x in range(2,lim):
		if is_prime(x):
			yield(x)

def list_primes_rec(lim=10):
	""" Recursive: Yield prime numbers below [lim]. """
	if lim > 1:
		for x in range(2,lim):
			for i in range(2, int(x ** 0.5) + 1):
				if x % i == 0:
					break
			else:
				yield x
			list_primes_rec(lim-1)

def main():
	for x in list_primes_rec(100):
		print(x)

if __name__ == '__main__':
	main()