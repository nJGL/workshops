""" Fizz-buzz with variable replacement dictionary to be extended with any number of even divides. """

def fizzbuzz(nr):
    nums = {'fizz': 3, 'buzz': 5}
    out = ''
    for replacement, numerator in nums.items():
        if nr/numerator == round(nr/numerator):
            out += replacement
    return(str(nr) if out == '' else out)

def main():
    for i in range(1, 25):
        print(fizzbuzz(i))

if __name__ == '__main__':
	main()