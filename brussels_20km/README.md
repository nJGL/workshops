# 20km sur Bruxelles

Having run the 20k in Brussels, I was interested in how I had been performing compared to my pairs. The web-page of the event generously provides the running times and additional variables for all runners and I took the liberty of scraping them to output some descriptive statistics.

In respectinig the anonymity of the runners, the data here-provided is anonymized. A very unorthodox scraping method using Applescript was used to get hold of the data in the first place.