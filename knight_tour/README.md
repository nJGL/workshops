# The knigt's tour

## Context

I watched the Queen's Gambit. My fascination for itterative random solutions to simple problems brought me to find a random night's tour generator.

The problem is explained [here](https://en.wikipedia.org/wiki/Knight%27s_tour). Basically, one is to move a night across every square on the table by only landing on each square once. It is similar to the Chinese postman-problem, and the concept of eulerisation of networks graphs, in which one tries to travel between nodes in a graph without traversing the same edge twice.

Euler (honor to his extraordinary genious), of course tacled this problem, and proposed some night's tours himself. After the bridges of Keunigsberg, I don't suppose he could help himself.

We mortals, who compare unfavourably to Euler (honoured be his memory), can read up on previous geniouses and find tachtics to discover the thousands of billions of valid knite's tours that exist. I, instead embarked on a differnt and wrote quickest night's run itterator that I could consieve in the gloomy afternoon of a failed morning date in late  november.

Start an empty board. Place a night. Move to a random valid new square, if there are any. Repeat as long as there are valid new sqares to move to and record the number of tries it takes to reach a valid knight's tour. Repeat.

This is stupid, you say. Yes, it truly is. But in theory, one chould be able to come across a valid tour every once in a while, right.

Not quite. I ran 6 million test, and found nothing. I must say that I expeceted a valid run to appear a bit earlier. What kind of result did I get? Well. Most often I stop at 62 moves, with unvisited squares in the corners of the board.

What would be the simplest optimization? Since the missing squares of almost good results are at the edges of the boards, where the number of possible squares to move to (all else equal) is smaller, the Warnsdorff's rule stated on the wikipedia-page seems to be a good idea.

Before applying any optimization in the knight's choice of square to move to, I recieved the followiing distribution of valid moves for the 6000000 unsuccefull itterations.

The most likely outcome appears to be that random trajectories of the knight reaches around 40 moves after which the probability to end up in a dead end increases steeply. Six million tries gave me 184 boards with 62 squares visited and only 41 boards with 63 squares visited.

At this point, one is tempted to believe that around 64 boards of 63 visited squares, the last square should be a valid one, but given that final unvisited squares tend to be in the boards of edges (and also given the general intuission about the structures that these itterations would produce), I expect the relation between the last two unvisited squares of this kind of iteration to be unevenly "distributed". I then began an inplementation of Warnsdorff's rule.

Warnsdorff's rule, states that one should always move the knight to a square where the number of valid squares to move to is the lowest, an thereby eliminate tricky-to-reach squares first.

Computationiolly, however, each iteration of moving the knight will have to evaluate the statuses of 8 times as many squares, to be able to choose a new square according to the rule. Any gains in the increase of correct knight tours that is found by the random itterations using the rule would have to be higher than that computational loss. But at 0 successfull runs over 6 milion tries, there was really nothing to loose.

The impact of the Warnsdorff's rule surprised me greatly.

The results are shocking! Not only does finding an accurate cycle become the likely outcome a much more likely outcome of a try, it becomes the most likely outcome by far. I ran a couple of hundred thousand tries, and ended up with XX% valid knight-runs. Fantastic!

These are the kind of optimizations that one needs in life also. Simple rules to be able to make radically better choices.

## Code directory

The code is written in Python and is rather self-explainatory.

