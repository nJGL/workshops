from random import randint, choice

# Walk always to the square with the least amount of posibilities
warnsdorffs_rule = True
output_file = 'output_with_rule.csv'


class Chessboard:
	def __init__(self):
		""" Build chessboard as matrix of single-character strings. """
		self.board = [['•','•','•','•','•','•','•','•'],
	                  ['•','•','•','•','•','•','•','•'],
					  ['•','•','•','•','•','•','•','•'],
					  ['•','•','•','•','•','•','•','•'],
					  ['•','•','•','•','•','•','•','•'],
					  ['•','•','•','•','•','•','•','•'],
					  ['•','•','•','•','•','•','•','•'],
					  ['•','•','•','•','•','•','•','•']]
	
	def add_piece(self, piece, coord):
		""" Add piece to square. """
		x, y = coord
		self.board[x][y] = str(piece)
	
	def piece_of(self, coord):
		return(self.board[ coord[0] ][ coord[1] ])

	def is_valid_coord(self, coord):
		""" Is this coordinate valid on the board. """
		x, y = coord
		if(x < 0 or y < 0 or x > 7 or y > 7):
			return(False)
		if(self.board[x][y] == '•'):
			return(True)
		else:
			return(False)


	def get_possible_relocations(self, coord):
		piece = self.piece_of(coord)
		x, y = coord

		new_coords = []
		# Behave as night
		new_coords.append( (x+2, y+1) )
		new_coords.append( (x+2, y-1) )
		new_coords.append( (x-2, y+1) )
		new_coords.append( (x-2, y-1) )
		new_coords.append( (x+1, y+2) )
		new_coords.append( (x-1, y+2) )
		new_coords.append( (x+1, y-2) )
		new_coords.append( (x-1, y-2) )
		
		new_coords = [x for x in new_coords if self.is_valid_coord(x)]
		
		return(new_coords)
	
	def print(self):
		""" Output board."""
		for row in self.board:
			for col in row:
				print(' '*(3-len(col)), col, end='')
			print('')




f = open(output_file, "a")

for attempt in range(10000):
	# Put knight on a random square of an empty board
	chess = Chessboard()
	i_coord = ( randint(0,7), randint(0,7) )
	chess.add_piece(1, (i_coord[0], i_coord[1]) )

	for i in range(2,64+1):
		moves = chess.get_possible_relocations( i_coord )

		if len(moves) == 0:
			# No possible moves. Night run failed
			f.write(str(i) + '\n')
			break
		else:
			# Update current square and write the iteration-number on that square
			if warnsdorffs_rule == True:
				# Eevaluate each move
				move_quality = [ len(chess.get_possible_relocations( move )) for move in moves]
				# Watch out for 0s. Lowest number of POSIBILITIES is not where there are 0 posibilities :)
				warnsdorff_moves = [x for x in move_quality if x != 0]
				if len(warnsdorff_moves) == 0:
					# We will reach a dead end on next iteration. Fair, enough. :/
					good_moves = moves
				else:
					good_moves = [moves[x] for x in range(len(moves)) if move_quality[x] == min(warnsdorff_moves) ]
				
				i_coord = choice(good_moves)

			else:
				# No evaluation of moves. Just go to a random possible square
				i_coord = choice(moves)

			# Brand that square with the iteration number
			chess.board[ i_coord[0] ][ i_coord[1] ] = str(i)

			# Did we manage to go all the way?

		if i == 64:
			f.write(str(i) + '\n')

f.close()