# Make Eulerian Graphs in iGraph

Originally posted as a response to a problem given on [stackoverflow](https://stackoverflow.com/questions/40576910/solving-chinese-postman-algorithm-with-eulerization).

# Eularian Graphs
Carl Hierholzer (1873) had explained how eulirian cycles exist for graphs that are 1) connected, and 2) contain only vertecies with even degrees. Based on this proof the posibility of an eulerian cycle existing in a graph can be tested by testing on these two conditions.

This function assumes a connected graph. It adds edges to a graph to ensure that all nodes eventuall has an even numbered. It tries to maintain the structure of the graph by primarily adding duplicates of already existing edges, but can also add "structurally new" edges if the structure of the graph does not all