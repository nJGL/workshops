# Eurovision Voting Clusters

This set of scrips in R analyse the clusters of mutual voting-relations over time in the Eurovision Song Contest.

In the Eurovision song context, countries apply telephone voting since 1998. The voting patterns, in terms of what countries that vote for what other countries, is stated in the Wikipedia-page on Eurovision-voting, to contain cultural cluster-effects.

### File structure
  - `src` contains scripts in R to mine and analyse data
  - `data` contains downloaded data to avoid mining at every runtime
  - `output` contains output-data

