# Monte Carlo explorations

Monte Carlo methods are typically used to model probabilities of difficult-to-calculate outcomes. The .5 probability of a coin flip could be calculated by repeatedly flipping a coin and observe the convergence towards .5. Monte Carloa approaches can thereby be used to verify known probabilities.

In the example with the birthday paradox, the known probability seems contra-intuitive, and the Monte Carlo simulation serves as a brute-force verification of the fact that it is likely that at least two people in a group of at least 24 people share the same birthday.

In the example to estimate pi, a Monte Carlo approach is applied to the relation between cube and sphere areas wherein pi can be algebraically isolated in an equation that allows for random data to fill the unknown elements and thereby arrive at an increasinly precise estimation of pi.

## Current workshops

 - **birthday_paradox** Comparing probabalistic and Monte Carlo method approaches to verifying the birthday paradox.

 - **estimation_of_pi:** Brute-forcing the estimation of pi using square-to-circle area ratios as described [here](https://en.wikipedia.org/wiki/Monte_Carlo_method).

   

   