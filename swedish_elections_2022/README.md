# Swedish general elections 2022

The Swedish election authorities kindly make available a large amount of statistics and data on electoral results and procedures. This directory contain my brush-up on qgis and some network descriptives on municipal coalitions formed during the previous election cycle. They may be used to underpin several of the discussions regarding the complicated parlimentary situation arising after the 2022 election to the national pairlament.

## Source material

**Original polygon data** of the electoral districts is provided by the Swedish electoral authorities ([Valmyndigheten](https://www.val.se/valresultat/riksdag-region-och-kommun/2022/radata-och-statistik.html)) - see the "Kartor över valdistrikt i val 2022" for a [full file of json data](https://www.val.se/download/18.75995f7b17f5a986a4ee30/1647001459842/valdistriktsfiler-alla-lan.zip) to be mapped in SWEREF99-projection.

**Municipal gouvernance data** has been assembeled by SKR (Swedish Municipalities and Regions) and [downloaded here](https://skr.se/download/18.5454beb81808a4df36f49833/1652256315297/Styre-kommun-uppdat-20210427.xlsx)

## Data

`data/bordering_district_edgelist.txt`  - an edgelist generated from a shape-file of the polygons of electoral districts for 2022. Source and target nodes are named by their official electoral district code. The weight is an arbitrary number relating to the number of overlapping coordinate points - not to be confused with an actual measure of the geographic distance of district borders which would have to be calculated differently. 

`data/municipal_covernance.csv`  - a raw `.csv` file of the list of governing coalitions in Swedish municipalities maintained by SKR mentioned above.
